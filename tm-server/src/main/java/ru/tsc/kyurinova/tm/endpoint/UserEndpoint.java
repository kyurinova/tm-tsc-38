package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    public UserEndpoint(IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull List<User> findAllUser(
            @Nullable
            @WebParam(name = "session")
                    Session session
    ) {
        sessionService.validate(session);
        return userService.findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User findByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User findByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return userService.findByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return userService.existsById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return userService.existsByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User findByEmailUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session);
        return userService.findByEmail(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isLoginExistsUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session);
        userService.isLoginExists(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isEmailExists(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session);
        userService.isEmailExists(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User findByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session);
        return userService.findByLogin(login);
    }

}
