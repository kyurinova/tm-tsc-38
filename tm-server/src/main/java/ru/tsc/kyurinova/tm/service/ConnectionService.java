package ru.tsc.kyurinova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = propertyService.getJdbcUser();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
