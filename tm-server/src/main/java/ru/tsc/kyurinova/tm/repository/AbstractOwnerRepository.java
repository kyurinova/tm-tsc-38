package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IOwnerRepository;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.model.AbstractOwnerEntity;
import ru.tsc.kyurinova.tm.model.Project;

import java.sql.*;
import java.util.*;
import java.util.Date;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull String comparator) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? ORDER BY ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, Sort.valueOf(comparator).name());
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }


    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String id = findByIndex(userId, index).getId();
        removeById(userId, id);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @Nullable final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, final Integer index) throws SQLException {
        findByIndex(userId, index);
        return true;
    }

    public void add(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (row_id, name, descr, user_id, status, created, start_dt, finish_dt VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        @Nullable final Date startDate = project.getStartDate();
        @Nullable final Date finishDate = project.getFinishDate();
        statement.setTimestamp(7, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setTimestamp(8, finishDate == null ? null : new Timestamp(finishDate.getTime()));
        statement.executeUpdate();
        statement.close();
    }

}
