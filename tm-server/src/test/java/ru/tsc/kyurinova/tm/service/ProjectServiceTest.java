package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.service.IProjectService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.repository.ProjectRepository;

import java.sql.SQLException;

public class ProjectServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "Description of project";

    @NotNull
    private final String userId;

    public ProjectServiceTest() throws SQLException {
        projectService = new ProjectService(new ConnectionService(new PropertyService()), new LogService());
        userService = new UserService((new ConnectionService(new PropertyService())), new LogService(), new PropertyService());
        userId = userService.create("test", "test").getId();
        project = projectService.create(userId, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    public void createByNameTest() throws SQLException {
        @NotNull final String newProjectName = "newTestProject";
        final Project newProject;
        newProject = projectService.create(userId, newProjectName);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
        projectService.remove(newProject);
    }

    @Test
    public void createTest() throws SQLException {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        final Project newProject;
        newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
        Assert.assertEquals(newProjectDescription, projectService.findByName(userId, newProjectName).getDescription());
        projectService.remove(newProject);
    }

    @Test
    public void findByProjectTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project.getId(), projectService.findById(userId, projectId).getId());
        Assert.assertEquals(project.getId(), projectService.findByIndex(userId, 0).getId());
        Assert.assertEquals(project.getId(), projectService.findByName(userId, projectName).getId());
    }

    @Test
    public void existsProjectTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertTrue(projectService.existsById(userId, projectId));
        Assert.assertTrue(projectService.existsByIndex(userId, 0));
    }

    @Test
    public void updateByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project.getId(), projectService.findByName(userId, projectName).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project.getId(), projectService.findByName(userId, newProjectName).getId());
        @NotNull final Project newProject = projectService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void updateByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project.getId(), projectService.findById(userId, projectId).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(project.getId(), projectService.findByIndex(userId, 0).getId());
        @NotNull final Project newProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void removeProjectByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.removeById(userId, projectId);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectService.removeByIndex(userId, 0);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeProjectByNameTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.removeByName(userId, projectName);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() throws SQLException {
        projectService.removeById(projectId);
        userService.removeById(userId);
    }

}
