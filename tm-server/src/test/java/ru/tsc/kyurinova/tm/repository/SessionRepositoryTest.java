package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class SessionRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        @NotNull final User user = new User();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        connection.commit();
        sessionRepository = new SessionRepository(connection);
        session = new Session();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
    }

    @Before
    public void before() throws SQLException {
    }

    @Test
    public void existsSessionTest() throws SQLException {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.exists(sessionId));
    }

    @Test
    public void removeSessionByIdTest() throws SQLException {
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionId);
        sessionRepository.removeById(sessionId);
        connection.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() throws SQLException {
        sessionRepository.clear();
        connection.commit();
        connection.close();
    }

}
