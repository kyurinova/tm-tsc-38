package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    // void add(E entity);

    // void addAll(@NotNull List<E> list);

    void remove(E entity) throws SQLException;

    @NotNull
    List<E> findAll() throws SQLException;

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator) throws SQLException;

    void clear() throws SQLException;

    @Nullable
    E findById(@Nullable String id) throws SQLException;

    @NotNull
    E findByIndex(@Nullable Integer index) throws SQLException;

    void removeById(@Nullable String id) throws SQLException;

    void removeByIndex(@Nullable Integer index) throws SQLException;

    boolean existsById(@Nullable String id) throws SQLException;

    boolean existsByIndex(@Nullable Integer index) throws SQLException;

    int getSize() throws SQLException;
}
