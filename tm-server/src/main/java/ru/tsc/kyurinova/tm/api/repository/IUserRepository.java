package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void add(@NotNull User user) throws SQLException;

    void addAll(@NotNull List<User> users) throws SQLException;

    @Nullable
    User findByLogin(@NotNull String login) throws SQLException;

    @Nullable
    User findByEmail(@NotNull String email) throws SQLException;

    @NotNull Boolean isLoginExists(@NotNull String login) throws SQLException;

    @NotNull Boolean isEmailExists(@NotNull String email) throws SQLException;

    void setPassword(@NotNull String userId, @NotNull String password) throws SQLException;

    void updateUser(@NotNull String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws SQLException;

    void setLockedByLogin(@NotNull String login, @NotNull String lockFlag) throws SQLException;

    @Override
    void removeById(@NotNull String id) throws SQLException;

    void removeByLogin(@NotNull String login) throws SQLException;

}
