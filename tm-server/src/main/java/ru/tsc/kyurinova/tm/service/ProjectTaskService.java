package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IProjectTaskService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.repository.ProjectRepository;
import ru.tsc.kyurinova.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }


    @Override
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        try {
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        try {
            taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        try {
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

}
