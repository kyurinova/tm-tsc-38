
package ru.tsc.kyurinova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeTaskUserId complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="removeTaskUserId"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.tm.kyurinova.tsc.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="entity" type="{http://endpoint.tm.kyurinova.tsc.ru/}task" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeTaskUserId", propOrder = {
        "session",
        "entity"
})
public class RemoveTaskUserId {

    protected Session session;
    protected Task entity;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the entity property.
     *
     * @return possible object is
     * {@link Task }
     */
    public Task getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     *
     * @param value allowed object is
     *              {@link Task }
     */
    public void setEntity(Task value) {
        this.entity = value;
    }

}
