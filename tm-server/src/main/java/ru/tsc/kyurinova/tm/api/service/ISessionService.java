package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.Session;

import java.sql.SQLException;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session open(@NotNull String login, @NotNull String password) throws SQLException;

    @NotNull
    Session sign(@NotNull Session session) throws SQLException;

    void close(@Nullable Session session) throws SQLException;

    @NotNull
    boolean checkDataAccess(@NotNull String login, @NotNull String password) throws SQLException;

    abstract void validate(@NotNull Session session) throws SQLException;

    void validate(@NotNull Session session, @NotNull Role role) throws SQLException;


}
