package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void addAll(@NotNull List<Project> projects) throws SQLException;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws SQLException;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws SQLException;

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void removeByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) throws SQLException;

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description) throws SQLException;

    void startById(@Nullable String userId, @Nullable String id) throws SQLException;

    void startByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void startByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void finishById(@Nullable String userId, @Nullable String id) throws SQLException;

    void finishByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void finishByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws SQLException;

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws SQLException;

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) throws SQLException;

}
