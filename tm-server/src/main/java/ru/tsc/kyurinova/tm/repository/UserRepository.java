package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.model.User;

import java.sql.*;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final static String USER_TABLE = "users";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return USER_TABLE;
    }

    @NotNull
    @Override
    protected User fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("row_id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("fst_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("mid_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getString("locked"));
        return user;
    }

    @Override
    public void add(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (row_id, login, password_hash, email, fst_name, last_name, mid_name, role, locked)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getRole().toString());
        statement.setString(9, user.getLocked());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NotNull List<User> users) throws SQLException {
        for (User user : users) {
            add(user);
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }


    @Nullable
    @Override
    public User findByEmail(@NotNull String email) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) throws SQLException {
        try {
            findByLogin(login);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) throws SQLException {
        try {
            findByLogin(email);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @Override
    public void setPassword(@NotNull String userId, @NotNull String password) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET password_hash = ? WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, password);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateUser(@NotNull String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET fst_name = ?, last_name = ?, mid_name = ? WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, firstName);
        statement.setString(2, lastName);
        statement.setString(3, middleName);
        statement.setString(4, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void setLockedByLogin(@NotNull String login, @NotNull String lockFlag) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET locked = ? WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, lockFlag);
        statement.setString(2, login);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeById(@NotNull String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByLogin(@NotNull String login) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

}
