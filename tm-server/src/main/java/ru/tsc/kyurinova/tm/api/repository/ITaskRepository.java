package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task) throws SQLException;

    void addAll(@NotNull List<Task> tasks) throws SQLException;

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name) throws SQLException;

    void removeByName(@NotNull String userId, @NotNull String name) throws SQLException;

    void startById(@NotNull String userId, @NotNull String id) throws SQLException;

    void startByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    void startByName(@NotNull String userId, @NotNull String name) throws SQLException;

    void finishById(@NotNull String userId, @NotNull String id) throws SQLException;

    void finishByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    void finishByName(@NotNull String userId, @NotNull String name) throws SQLException;

    void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws SQLException;

    void updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description) throws SQLException;

    void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws SQLException;

    void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) throws SQLException;

    void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) throws SQLException;

    void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws SQLException;

    void unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws SQLException;

    @Nullable
    Task findByProjectAndTaskId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws SQLException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;
}
