package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    //   void addAll(@NotNull List<E> list) throws SQLException;

    void remove(final E entity) throws SQLException;

    @NotNull
    List<E> findAll() throws SQLException;

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator) throws SQLException;

    void clear() throws SQLException;

    @Nullable
    E findById(@NotNull String id) throws SQLException;

    @NotNull
    E findByIndex(@NotNull Integer index) throws SQLException;

    void removeById(@NotNull String id) throws SQLException;

    void removeByIndex(@NotNull Integer index) throws SQLException;

    boolean existsById(@NotNull String id) throws SQLException;

    boolean existsByIndex(@NotNull Integer index) throws SQLException;

    int getSize() throws SQLException;
}
