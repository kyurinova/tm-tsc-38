package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;

import java.sql.*;
import java.util.List;
import java.util.Date;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    private final static String PROJECT_TABLE = "project";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return PROJECT_TABLE;
    }

    @NotNull
    @Override
    protected Project fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("row_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("descr"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        project.setStartDate(row.getTimestamp("start_dt"));
        project.setFinishDate(row.getTimestamp("finish_dt"));
        return project;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (row_id, name, descr, user_id, status, created, start_dt, finish_dt) VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        @Nullable final Date startDate = project.getStartDate();
        @Nullable final Date finishDate = project.getFinishDate();
        statement.setTimestamp(7, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setTimestamp(8, startDate == null ? null : new Timestamp(finishDate.getTime()));
        statement.executeUpdate();
        statement.close();
    }


    @Override
    public void addAll(@NotNull List<Project> projects) throws SQLException {
        for (Project project : projects) {
            add(project.getUserId(), project);
        }
    }


    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE row_id = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setInt(4, index);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE name = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, name);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE row_id = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setInt(4, index);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE name = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, name);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, descr = ?" +
                " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, descr = ?" +
                " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }


    @Override
    public void changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startById(userId, id);
        else if (Status.COMPLETED.toString().equals(status))
            finishById(userId, id);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND row_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setString(3, id);
            statement.executeUpdate();
            statement.close();
        }
    }

    @Override
    public void changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startByIndex(userId, index);
        else if (Status.COMPLETED.toString().equals(status))
            finishByIndex(userId, index);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setInt(3, index);
            statement.executeUpdate();
            statement.close();
        }
    }

    @Override
    public void changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startByName(userId, name);
        else if (Status.COMPLETED.toString().equals(status))
            finishByName(userId, name);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND name = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setString(3, name);
            statement.executeUpdate();
            statement.close();
        }
    }

}
