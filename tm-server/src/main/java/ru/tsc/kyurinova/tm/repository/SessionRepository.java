package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.model.Session;

import java.sql.*;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final static String SESSION_TABLE = "session";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return SESSION_TABLE;
    }

    @NotNull
    @Override
    protected Session fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("row_id"));
        session.setUserId(row.getString("user_id"));
        session.setTimestamp(row.getTimestamp("time_stamp").getTime());
        session.setSignature(row.getString("signature"));
        return session;
    }

    @Override
    public void add(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (row_id, user_id, signature, time_stamp) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setTimestamp(4, new Timestamp(session.getTimestamp()));
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public boolean exists(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean result = resultSet.next();
        statement.close();
        return result;
    }

}
