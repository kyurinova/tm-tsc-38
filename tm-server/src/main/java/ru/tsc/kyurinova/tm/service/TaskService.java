package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        super(connectionService, logService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public void addAll(@NotNull final List<Task> tasks) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.addAll(tasks);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }


    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task(userId, name, "");
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.add(userId, task);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task(userId, name, description);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.add(userId, task);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            return taskRepository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }

    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.startByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.finishById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.finishByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.finishByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusByIndex(userId, index, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            taskRepository.changeStatusByName(userId, name, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Task findByProjectAndTaskId(@Nullable final String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getRepository(connection);
        try {
            return taskRepository.findByProjectAndTaskId(userId, projectId, taskId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }


}
