package ru.tsc.kyurinova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Close application...";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
