package ru.tsc.kyurinova.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStartDate, IHasStatus {
}
