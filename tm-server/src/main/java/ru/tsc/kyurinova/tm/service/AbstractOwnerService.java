package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IOwnerRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IOwnerService;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    public AbstractOwnerService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        super(connectionService, logService);
    }

    @NotNull
    protected abstract IOwnerRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public void remove(@Nullable final String userId, @Nullable final E entity) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.remove(userId, entity);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findAll(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable String comparator) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findAll(userId, comparator);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.clear(userId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.removeById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.existsById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @NotNull final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.existsByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

}
