package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    void addAll(@NotNull List<Task> tasks) throws SQLException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws SQLException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws SQLException;

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void removeByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void updateById(@Nullable String userId, @Nullable String id, String name, @NotNull String description) throws SQLException;

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description) throws SQLException;

    void startById(@Nullable String userId, @Nullable String id) throws SQLException;

    void startByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void startByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void finishById(@Nullable String userId, @Nullable String id) throws SQLException;

    void finishByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void finishByName(@Nullable String userId, @Nullable String name) throws SQLException;

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws SQLException;

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws SQLException;

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) throws SQLException;

    @Nullable
    Task findByProjectAndTaskId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

}
