package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class UserRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.ru";

    public UserRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt("testUser", 3, "test"));
    }

    @Before
    public void before() throws SQLException {
        userRepository.add(user);
        connection.commit();
    }

    @Test
    public void findByUserTest() throws SQLException {
        Assert.assertEquals(user.getId(), userRepository.findById(userId).getId());
        Assert.assertEquals(user.getId(), userRepository.findByIndex(0).getId());
        Assert.assertEquals(user.getId(), userRepository.findByLogin(userLogin).getId());
        Assert.assertEquals(user.getId(), userRepository.findByEmail(userEmail).getId());
    }

    @Test
    public void removeUserTest() throws SQLException {
        Assert.assertNotNull(user);
        userRepository.remove(user);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userLogin);
        userRepository.removeByLogin(userLogin);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdTest() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userId);
        userRepository.removeById(userId);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdIndex() throws SQLException {
        Assert.assertNotNull(user);
        userRepository.removeByIndex(0);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() throws SQLException {
        userRepository.clear();
        connection.commit();
        connection.close();
    }

}
